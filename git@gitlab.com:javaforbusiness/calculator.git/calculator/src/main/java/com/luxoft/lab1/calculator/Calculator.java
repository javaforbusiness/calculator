package com.luxoft.lab1.calculator;

public class Calculator {
	private static final int MAX_NUMBER = 100;
	
	public int sum(String numbers) {
		if(numbers.equals("")){
			return 0;
		}
		String[] splitedNumbers = numbers.split(",");
		int result = 0;
		for(String number: splitedNumbers) {
			int parsedNumber = Integer.parseInt(number);
			if (parsedNumber >= 0) {
				result+= parsedNumber;
			}
		}				
		throwExceptionForToBigResult(result);		
		return result;
	}
	
	private void throwExceptionForToBigResult(int result) {
		if(result>MAX_NUMBER) {
			throw new IllegalArgumentException();
		}
	}
	
	
    
}
