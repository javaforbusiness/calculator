package com.luxoft.lab1.calculator;

public class Translator {
	
	public String translate(String number) {
		Calculator calc = new Calculator();
		String stringToReplace = number;
		stringToReplace = stringToReplace.replace("IV", "4,");
		stringToReplace = stringToReplace.replace("IX", "9,");
		stringToReplace = stringToReplace.replace("XL", "40,");
		stringToReplace = stringToReplace.replace("I", "1,");
		stringToReplace = stringToReplace.replace("V", "5,");
		stringToReplace = stringToReplace.replace("X", "10,");
		stringToReplace = stringToReplace.replace("L", "50,");
		Integer result = calc.sum(stringToReplace);
		return result.toString();
	}
}
