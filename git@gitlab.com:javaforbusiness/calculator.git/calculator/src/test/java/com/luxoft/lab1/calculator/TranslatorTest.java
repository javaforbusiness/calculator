package com.luxoft.lab1.calculator;

import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;


public class TranslatorTest {
	Translator translator = new Translator();
	@Test
	public void shouldConvertXtoArablic() {
		
		
		Calculator calculatorMock = mock(Calculator.class);
		when(calculatorMock.sum("10,")).thenReturn(11);
		String result = translator.translate("X");
		
		Assert.assertEquals("10" ,result);
	}
	
	@Test
	public void shouldConvertXXIIItoArablic() {
		
		//Assert.assertEquals("23" ,result);
	}
	
	@Test
	public void shouldConvert36toArablic() {
		
		//Assert.assertEquals("36" ,result);
	}
}
