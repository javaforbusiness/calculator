package com.luxoft.lab1.calculator;

import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class CalculatorTest {
	Calculator calculator = new Calculator(); 
	
	@Test
	public void shouldReturnNullForEmptyString() {
		int sum = calculator.sum("");
		
		Assert.assertEquals(0, sum);		
	}
	
	@Test
	public void shouldReturnTheSameNumberIfThereIsOnlyOne() {
		int sum = calculator.sum("5");
		
		Assert.assertEquals(5, sum);		
	}
	
	@Test
	public void shouldReturnSumOfTwoNumbers() {
		int sum = calculator.sum("2,4");
		
		Assert.assertEquals(6, sum);		
	}
	
	@Test
	public void shouldCountSumOfMoreThanTwoNumbers() {
		int sum = calculator.sum("1,2,3,4");
		
		Assert.assertEquals(10, sum);		
	}
	
	@Test
	public void shouldIgnoreNegativeNumbers() {
		int sum = calculator.sum("1,2,-3");
		
		Assert.assertEquals(3, sum);		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSumIsBiggerThanOneHundred() {
		calculator.sum("50,51");
	}

}